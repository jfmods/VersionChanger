package io.gitlab.jfronny.versionchanger;

import io.gitlab.jfronny.libjf.config.api.v2.Entry;
import io.gitlab.jfronny.libjf.config.api.v2.JfConfig;

@JfConfig
public class Cfg  {
    @Entry public static String version = "1.7.10 Definitive Edition";

    static {
        JFC_Cfg.ensureInitialized();
    }
}
