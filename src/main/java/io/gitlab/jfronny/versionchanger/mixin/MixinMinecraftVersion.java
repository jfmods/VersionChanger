package io.gitlab.jfronny.versionchanger.mixin;

import com.google.gson.JsonObject;
import io.gitlab.jfronny.versionchanger.Cfg;
import net.minecraft.MinecraftVersion;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MinecraftVersion.class)
public class MixinMinecraftVersion {
    @Mutable
    @Final
    @Shadow
    private String name;
    @Inject(at = @At("RETURN"), method = "<init>()V")
    public void init(CallbackInfo info) {
        name = Cfg.version;
    }

    @Inject(at = @At("RETURN"), method = "<init>(Lcom/google/gson/JsonObject;)V")
    public void init(JsonObject jsonObject, CallbackInfo info) {
        init(info);
    }
}
