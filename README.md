VersionChanger allows modifying the displayed minecraft version with a simple config file.

Any normal text is supported, not just numbers.

Please note that some mods may also read the version and give strange output.